const styles = theme => ({
    paperStyle: {
        flexGrow: 1,
        borderRadius: 20,
        boxShadow: '0 2px 12px 0 rgba(0,0,0,0.14)'
    },
    swapHeading: {
        fontWeight: 'bold',
        marginTop: '1em',
        letterSpacing: 2.5,
        color: '#323C47',
        fontSize: 13
    },
    swapContent: {
        textAlign: 'left',
        marginTop: '1em',
        letterSpacing: .5,
        color: '#1C2731',
        fontSize: 13,
        width: 300,
        lineHeight: 1.3,
        fontWeight: 500
    },
    buttonSwap: {
        width: 126,
        height: 36,
        backgroundColor: '#FFFFFF',
        boxShadow: '0 2px 10px 0 rgba(51,204,255,0.4)',
        border: '1px solid #33CCFF'
    },
    avatarUserStyle: {
        height: 55,
        width: 55,
        paddingTop: 30,
        paddingBottom: 30,
    },
    avatarArrow: {
        width: 60,
        height: 40,
    },
    avatarOvalGreen: {
        height: 92,
        width: 92
    },
    buttonStyle: {color: '#33CCFF', fontSize: 11, fontWeight: 'bold', letterSpacing: 3, width: 80, paddingTop: 7},
    buttonArrowStyle: {width: 20}
});

export default styles
