const styles = theme => ({
    paperStyle: {
        flexGrow: 1,
        borderRadius: 20,
        boxShadow: '0 2px 12px 0 rgba(0,0,0,0.14)'
    },
    buttonPool: {
        width: 126,
        height: 36,
        backgroundColor: '#FFFFFF',
        boxShadow: '0 2px 10px 0 rgba(51,204,255,0.4)',
        border: '1px solid #33CCFF'
    },
    buttonDisabled: {
        width: 126,
        height: 36,
        backgroundColor: '#FFFFFF',
        border: '1px solid #E8E8E8',
        shadowOffset: {},
        boxSizing: 'border-box'
    },
    poolHeading: {
        fontWeight: 'bold',
        marginTop: '1em',
        letterSpacing: 2.5,
        color: '#323C47',
        fontSize: 13
    },
    poolContent: {
        textAlign: 'left',
        marginTop: '1em',
        letterSpacing: .5,
        color: '#1C2731',
        fontSize: 13,
        width: 300,
        lineHeight: 1.3,
        fontWeight: 500
    },
    backButtonPool: {
        color: '#9B9B9B',
        fontSize: 11,
        fontWeight: 'bold',
        letterSpacing: 3,
        textAlign: 'center',
        width: 80,
        paddingTop: 4
    },
    avatarArrowGreenStyle: {
        height: 45,
        width: 80,
        paddingTop: 20
    },
    avatarRoundGreenStyle: {
        height: 92,
        width: 92
    },
    avatarYellowArrowStyle: {
        height: 45,
        width: 80,
        paddingTop: 15
    },
    coinStyleBNB: {
        color: '#323C47',
        fontSize: 18,
        fontWeight: 600,
        letterSpacing: 2.55
    },
    poolButtonStyle: {color: '#33CCFF', fontSize: 11, fontWeight: 'bold', letterSpacing: 3, width: 80, paddingTop: 7},
    poolButtonArrowStyle: {
        width: 20
    }
});

export default styles
