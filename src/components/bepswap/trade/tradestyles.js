const styles = theme => ({
    paperStyle: {
        borderRadius: 20,
        flexGrow: 1,
        boxShadow: '0 2px 12px 0 rgba(0,0,0,0.14)'
    },
    buttonSwap: {
        width: 126,
        height: 36,
        backgroundColor: '#FFFFFF',
        boxShadow: '0 2px 10px 0 rgba(51,204,255,0.4)',
        border: '1px solid #33CCFF'
    },
    buttonDisabled: {
        width: 126,
        height: 36,
        backgroundColor: '#FFFFFF',
        border: '1px solid #E8E8E8',
        shadowOffset: {},
        boxSizing: 'border-box'
    },
    tradeHeading: {
        fontWeight: 'bold',
        marginTop: '1em',
        letterSpacing: 2.5,
        color: '#323C47',
        fontSize: 13
    },
    tradeContentRightStyle: {
        textAlign: 'left',
        marginTop: '1em',
        letterSpacing: .5,
        color: '#1C2731',
        fontSize: 13,
        width: 300,
        lineHeight: 1.3,
        fontWeight: 500
    },
    tradeBackButtonStyle: {
        color: '#9B9B9B',
        fontSize: 11,
        fontWeight: 'bold',
        letterSpacing: 3,
        textAlign: 'center',
        width: 80,
        paddingTop: 4
    },
    avatarContentPool: {
        width: 100,
        fontSize: 18,
        fontWeight: 600,
        letterSpacing: 2.25
    },
    avatarContentPrice: {
        width: 90,
        fontSize: 18,
        fontWeight: 600,
        letterSpacing: 2.25,
        paddingTop: 10
    },
    avatarArrowStyle: {
        width: 50
    },
    avatarTradeUserStyle: {
        height: 55,
        width: 55
    },
    avatarArrowTwoWayStyle: {
        width: 50
    },
    avatarMarketStyle: {
        height: 65,
        width: 55,
        paddingLeft: 20
    },
    tradeButtonStyleFinish: {color: '#33CCFF', fontSize: 11, fontWeight: 'bold', letterSpacing: 3, width: 80, paddingTop: 7},
    tradeButtonArrow: {width: 20}
});

export default styles
