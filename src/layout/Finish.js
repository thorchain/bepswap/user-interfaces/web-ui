import React, {Component} from 'react'
import Typography from '@material-ui/core/Typography';

import {withStyles} from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'

import Paper from '@material-ui/core/Paper';
import {library} from '@fortawesome/fontawesome-svg-core';
import {
    faTwitterSquare,
    faTelegramPlane,
} from '@fortawesome/free-brands-svg-icons';

import styles from './HomeStyles';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Button from "@material-ui/core/Button/Button";
import {Link} from "react-router-dom";

library.add(faTwitterSquare, faTelegramPlane);

class Finish extends Component {

    render() {
        const {classes} = this.props;

        const thorchainTwitter = "https://twitter.com/thorchain_org?lang=en";
        const thorchainTelegram = "https://t.me/thorchain_org";

        return (
            <div >
                <Paper className={classes.paperStyle}>
                    <Grid
                        container
                        direction="column"
                        justify="flex-start"
                        alignItems="flex-start"
                    >
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: 10 }}>
                            </Grid>
                        </Grid>
                        <Grid container alignItems="center">
                            <Grid item xs >
                                <Typography gutterBottom variant="body1" className={classes.headerFinishContent}>
                                    THANK FOR USING BEPSWAP TUTORIAL
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container alignItems="center">
                            <Grid item xs style={{ padding: 10 }}>
                            </Grid>
                        </Grid>
                        <Grid container alignItems="center">
                            <Grid item xs >
                                <Typography gutterBottom variant="body1" className={classes.headerFinishContent}>
                                    PLEASE FOLLOW TWITTER AND TELEGRAM FOR MORE INFORMATION
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container spacing={1}>
                            <Grid item xs style={{ padding: '3em' }}>
                            </Grid>
                        </Grid>
                        <Grid container alignItems="center">
                        <Grid container spacing={5}>
                            <Grid item xs={3} >
                            </Grid>
                            <Grid item xs={2} >
                                <a href={thorchainTwitter}> <FontAwesomeIcon icon={faTwitterSquare} size="6x" style={{color: '#62D8E0'}} /></a>
                            </Grid>
                            <Grid item xs={2} >
                            </Grid>
                            <Grid item>
                                <a href={thorchainTelegram}> <FontAwesomeIcon icon={faTelegramPlane} size="6x" style={{color: '#62D8E0'}} /></a>
                            </Grid>
                            <Grid item xs={3} >
                            </Grid>
                        </Grid>

                            <Grid container spacing={1}>
                                <Grid item xs style={{ padding: 20 }}>
                                </Grid>
                            </Grid>
                            <Grid container spacing={1}>
                                <Grid item xs style={{ padding: 20 }}>
                                </Grid>
                                <Grid item xs style={{ padding: 20 }}>
                                </Grid>
                            </Grid>
                            <Grid container alignItems="center">
                                <Grid item xs >
                                    <Button className={classes.buttonDisabled} component={Link} to="/">
                                        <Grid item  >
                                            <Grid item>
                                                <Typography gutterBottom variant="body1" style={{color: '#9B9B9B', fontSize: 11, fontWeight: 'bold', letterSpacing: 3, marginTop: 6, textAlign: 'center', width: 80}}>
                                                    BACK</Typography>
                                            </Grid>
                                        </Grid>
                                    </Button>
                                </Grid>
                            </Grid>
                            <Grid container spacing={1}>
                                <Grid item xs style={{ padding: '1em' }}>
                                </Grid>
                            </Grid>
                    </Grid>
                    </Grid>
                </Paper>
            </div>
        )
    }
}

export default (withStyles(styles, {withTheme: true})(Finish))
