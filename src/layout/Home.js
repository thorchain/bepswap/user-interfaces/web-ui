import React, {Component} from 'react'
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import {withStyles} from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper';
import coinMith from './../assets/coins/coinMith.svg';
import coinOne from './../assets/coins/coinOne.svg';
import coinRune from './../assets/coins/coinRune.svg';
import coinTomob from './../assets/coins/coinTomob.svg';
import coinFtm from './../assets/coins/coinFtm.svg';
import coinAnkr from './../assets/coins/coinAnkr.svg';
import coinBolt from './../assets/coins/coinBolt.svg';
import coinBtc from './../assets/coins/coinBtc.svg';
import coinBnb from './../assets/coins/coinBnb.svg';
import coinElrond from './../assets/coins/coinElrond.svg';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

import styles from './HomeStyles';
import AppBar from "@material-ui/core/AppBar/AppBar";

class Home extends Component {

    render() {
        const {classes} = this.props;
        return (
            <AppBar position="static" className={classes.root}>
                <Paper className={classes.paperStyle}>
                    <Grid
                        container
                        direction="column"
                        justify="space-around"
                        alignItems="flex-start"
                    >
                        <Grid container alignItems="center">
                            <Grid item xs style={{ padding: 5 }}>

                            </Grid>
                        </Grid>
                        <Grid container alignItems="center">
                            <Grid item xs>
                                <Typography gutterBottom variant="body1" className={classes.headerContent}>
                                    INTRODUCING BEPSWAP
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Divider/>
                    <Grid container alignItems="center">
                        <Grid container spacing={3}>
                            <Grid item xs>
                                <Grid
                                    container
                                    direction="column"
                                    justify="space-around"
                                    alignItems="flex-start"
                                >
                                    <Grid container spacing={3}>
                                        <Grid item xs={2}>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <Avatar alt="Coin Rune" src={coinRune} className={classes.avatarStyle1} />
                                        </Grid>
                                        <Grid item xs={1} >
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3}>
                                        <Grid item xs>
                                        </Grid>
                                        <Grid item  >
                                            <Avatar alt="Coin Mith" src={coinMith} className={classes.avatarStyle2} />
                                        </Grid>
                                        <Grid item xs>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3}>
                                        <Grid item xs={2}>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <Avatar alt="Coin One" src={coinOne} className={classes.avatarStyle1} />
                                        </Grid>
                                        <Grid item  xs={1}  >
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3}>
                                        <Grid item xs>
                                        </Grid>
                                        <Grid item  >
                                            <Avatar alt="Coin Tomob" src={coinTomob} className={classes.avatarStyle2} />
                                        </Grid>
                                        <Grid item xs>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3}>
                                        <Grid item xs={2}>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <Avatar alt="Coin Btc" src={coinBtc} className={classes.avatarStyle1} />
                                        </Grid>
                                        <Grid item xs >
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={6}>
                                <Grid
                                    container
                                    direction="column"
                                    justify="flex-start"
                                    alignItems="flex-start"
                                >
                                    <Grid container alignItems="center">
                                        <Grid item xs>
                                            <Typography gutterBottom variant="body1" className={classes.subHeadingContent}>
                                                BEPSwap is UniSwap for Binance Chain. UniSwap is one of Ethereum's killer DeFi
                                                dapps
                                                with over $20m stakeed and $1m in daily volume
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid container alignItems="center">
                                        <Grid item xs>
                                            <Typography gutterBottom variant="body1" className={classes.subHeadingContent1}>
                                                BEPSwap will have the following features:
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid container alignItems="center">
                                        <Grid item xs>
                                            <Typography gutterBottom variant="body1" className={classes.headerContent2}>
                                                Token User:
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid container alignItems="center">
                                        <Grid item>
                                            <Typography gutterBottom variant="body1" className={classes.subHeadingContent1}>
                                                Instantly swap assets in your wallet, or swap and send them to a friend at
                                                market prices
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid container alignItems="center">
                                        <Grid item xs>
                                            <Typography gutterBottom variant="body1" className={classes.headerContent2}>
                                                Token Holders:
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid container alignItems="center">
                                        <Grid item>
                                            <Typography gutterBottom variant="body1" className={classes.subHeadingContent1}>
                                                Stake tokens in liquidity pools and earn commissions on every trade
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid container alignItems="center">
                                        <Grid item xs>
                                            <Typography gutterBottom variant="body1" className={classes.headerContent2}>
                                                Traders:
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid container alignItems="center">
                                        <Grid item>
                                            <Typography gutterBottom variant="body1" className={classes.subHeadingContent1}>
                                                Monitor pool prices and earn by trading the pools to correct prices.
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid container alignItems="center">
                                        <Grid item xs>
                                            <Typography gutterBottom variant="body1" className={classes.headerContent2}>
                                                Projects:
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid container alignItems="center">
                                        <Grid item>
                                            <Typography gutterBottom variant="body1" className={classes.subHeadingContent1}>
                                                Connect to BEPSwap to allow your token holders to instantly swap in and out of
                                                assets whilst using your DApp. Stake your treasury to instantly give your token
                                                deep
                                                liquidity and earn the trading fees.
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid container alignItems="center">
                                        <Grid item xs>
                                            <Button className={classes.button}  component={Link} to="/bepswap/swap">
                                                <Typography className={classes.buttonText}>START</Typography>
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item style={{paddingRight: 20}}></Grid>
                            <Grid item xs>
                                <Grid
                                    container
                                    direction="column"
                                    justify="flex-end"
                                    alignItems="flex-end"
                                >
                                    <Grid container spacing={3}>
                                        <Grid item xs={2}>
                                        </Grid>
                                        <Grid item xs={1}>
                                            <Avatar alt="Coin Ftm" src={coinFtm} className={classes.avatarStyle1} />
                                        </Grid>
                                        <Grid item xs={1} >
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3}>
                                        <Grid item xs>
                                        </Grid>
                                        <Grid item  >
                                            <Avatar alt="Coin Ankr" src={coinAnkr} className={classes.avatarStyle2}  />
                                        </Grid>
                                        <Grid item xs>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3}>
                                        <Grid item xs={2}>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <Avatar alt="Coin Bolt" src={coinBolt} className={classes.avatarStyle1} />
                                        </Grid>
                                        <Grid item  xs={1} >
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3}>
                                        <Grid item xs>
                                        </Grid>
                                        <Grid item  >
                                            <Avatar alt="Coin Bnb" src={coinBnb} className={classes.avatarStyle2} />
                                        </Grid>
                                        <Grid item xs>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3}>
                                        <Grid item xs={2}>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <Avatar alt="Coin Bnb" src={coinElrond} className={classes.avatarStyle2} />
                                        </Grid>
                                        <Grid item xs >
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Paper>
            </AppBar>

        )
    }
}

export default (withStyles(styles, {withTheme: true})(Home))
